float running_ratio(float exhaust_temp) {
  static unsigned long change_timer;
  //Not hot enough to start adjusting
  int fuel_target;
  int fuel_current;

  if(exhaust_temp > 75)
  {    
    fuel_current = fuel_need*100;
    
    if(water_temp*100 < shower_min*100)
      fuel_target = throttling_high_fuel*100;

    if(water_temp*100 >= (water_warning-2)*100)
      fuel_target = throttling_low_fuel*100;

    if(water_temp*100 >= shower_min*100 && water_temp*100 < (water_warning-2)*100)
      fuel_target = final_fuel*100;

    if(change_timer == 0)
      change_timer = millis();
     

    if(fuel_target != fuel_current)
    {
        
        if(millis() - change_timer >= 150)
        {
          if(fuel_current<fuel_target)
          {
            fuel_need += 0.01;
            message = "Running Increasing Fuel";
          }
          else if(fuel_current>fuel_target)
          {
            fuel_need -= 0.01;
            message = "Running Decreasing Fuel";
          }
          else
            message = "Running";
            
          change_timer = millis();
        }
    }
    else
      message = "Running";
  }
}
