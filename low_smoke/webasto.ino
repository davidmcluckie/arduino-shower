void webasto() { // this will handle the combustion
  static unsigned long timer;
  static unsigned long fan_timer;
  if (millis() < timer) {
    timer = millis();
  }
  static float temp_init;
  static int ignit_fail;
  static int seconds;
  static int cooled_off = 0;

  if (millis() > timer + 1000) { // every seconds, run this
    timer = millis();
    seconds ++; // increment the seconds counter
    logging(ignit_fail, temp_init, seconds);
  }

  if (!webasto_fail) { // if everything's going fine

    if ((burn_mode == 0 || burn_mode == 3) && burn) {  // if the user wants a shower
      // initiate the start sequence and retry
      burn_mode = 1;
      seconds = 0;
      glow_time = 0;
      temp_init = exhaust_temp; // store the exhaust temperature before trying to start the fire
      cooled_off = 1;
    }

    if ((burn_mode == 1 || burn_mode == 2) && !burn) { // if the shower has ended 
      burn_mode = 3;
      seconds = 0;
      ignit_fail = 0;
    }

    if (ignit_fail > 3) { // if there was more than 3 attempts to start fire but all failed
      webasto_fail = 1;
      burn_mode = 3;
      shower = 0;
      burn = 0;
    }

    if (exhaust_temp > 500) { // if overheating
      webasto_fail = 1;
      burn_mode = 3;
      shower = 0;
      burn = 0;
    }

  } else { // if there has been a major failure, stop everything
    shower = 0;
    cold_shower = 0;
    burn = 0;
    ignit_fail = 0;
    message = "Off";
  }

  switch (burn_mode) {
    case 0: { // everything is turned off in this mode
        fan_speed = 0;
        fuel_need = 0;
        glow_time = 0;
        lean_burn = 0;
      } break;

    case 1: { // the fire starting sequence
        if(webasto_fail)
          burn_mode = 3;
          
        if(exhaust_temp > 70 && (cooled_off == 0 || seconds < 5))
        {
          message = "Cooling < 70";
          fan_speed = 90;
          fuel_need = 0;
          seconds = 0;
          cooled_off = 0;
        }
        else if(exhaust_temp <= 70)
          cooled_off = 1;
        
        if(seconds > 0 && seconds < 5)
        {
          fan_speed = prime_fan_speed;
          glow_time = 105;
          fuel_need = 0;
          message = "Clearing Chamber";
        }

//        if(seconds >= 6 && seconds <= 10)
//        {
//          glow_time = 100;
//          message = "Glowing";
//        }

        if(seconds >= 6 && seconds <= 9)
        {
          fan_speed = 15;
          fuel_need = prime_ratio(exhaust_temp);
          message = "Prime";
        }

        if(seconds > 9 && seconds <= 11)
          fuel_need = 0;

        if (seconds >=40 && seconds <= 41)
        {
          temp_init = exhaust_temp;
          fan_timer = millis();
        }




        if (seconds > 17) { // the glow plug has just been turn of (7+12=19)
          fuel_need = start_fuel;
          message = "Firing Up";

          if(fan_speed < start_fan_speed)
          {
            if(millis() - fan_timer >= 333)
            {
              fan_speed += 1;
              fan_timer = millis();
            }
          }
          else
            fan_speed = start_fan_speed; // get some more air and restart pumping fuel slowly //60

          
          
        }

        if (exhaust_temp - temp_init > 3 && seconds >=50) { // exhaust temp raised a bit meaning fire has started //Debug this value of 0.5c is way too low maybe change it to 5c
          burn_mode = 2; // go to main burning mode and initialize variables
          seconds = 0;
          glow_time = 0;
          ignit_fail = 0;
          temp_init = exhaust_temp; 
          fan_timer = millis();
          message = "Started";

        }

        if ((seconds > 120 && ignit_fail > 0) || seconds > 120) {
          // the fire sequence didn't work, give it an other try
          burn_mode = 0;
          ignit_fail ++;
          glow_time = 5;
          cooled_off = 0;
          message = "Restarting";
        }

        if (exhaust_temp < exhaust_temp_sec[9]-1.0 && seconds >= 60) { // if flame died during burn
          burn_mode = 0;
          ignit_fail ++;
          cooled_off = 1;
          message = "Start Flameout";
        }

      } break;

    case 2: { // a really simple flame managment here, just get at full power

        //if ((exhaust_temp < 400 || water_temp < 50) && !webasto_fail) {
        if ( water_temp < water_overheat && !webasto_fail) {
         
          //Slowly speed up
          if (fan_speed<final_fan_speed) { 
            if(millis() - fan_timer >= 333)
            {
              message = "Increasing Burn";
              fan_speed += (final_fan_speed-35.00)/full_power_increment_time/3;
              fuel_need += (final_fuel-1.00)/full_power_increment_time/3;
              fan_timer = millis();
            }
          }
          else
          {
            //message = "Running";
            fan_speed = final_fan_speed; 
            running_ratio(exhaust_temp);                     
          }
        }

        if(webasto_fail || water_temp > water_overheat)
        {
          fan_speed = 100;
          fuel_need = 0;
          cooled_off = 1;
          message = "Overheating";
          burn = 0;
          shower = 0;
          burn_mode = 3;
        }

        if (exhaust_temp < 210 && exhaust_temp < exhaust_temp_sec[9]-3 && seconds >= 40) { // if flame died
          burn = 0;
          shower = 0;
          burn_mode = 3;
          message = "Running Flameout";
        }
 
      } break;

    case 3: { // snuff out the fire, with just a little air to avoid fumes and backfire

        fuel_need = 0;
        fan_speed = 100;
        glow_time = 30;
        message = "Shutting Down";
        if (seconds > 30 && exhaust_temp < 80) {
          burn_mode = 0;
          message = "Off";
          glow_time = 0;
        }
      } break;
  }
  fuel_pump();
  burn_fan();
  glow_plug();
}
