//Andrews Mod

#include <math.h> // needed to perform some calculations

//Pin Connections
int fuel_pump_pin = 5;
int glow_plug_pin = 6;
int burn_fan_pin = 9;
int water_pump_pin = 10;
int water_temp_pin = A0;
int exhaust_temp_pin = A1;
int push_pin = 13;
//Pin Connections

//Hardware Config
int pump_size = 22; //22,30,60 //todo later use this
bool high_temp_exhaust_sensor = false;
//Hardware Config

//Fuel Mixture 
//Prime
int prime_fan_speed = 15;
int prime_low_temp = 0;
int prime_low_temp_fuelrate = 3.5;
int prime_high_temp = 20;
int prime_high_temp_fuelrate = 2.0;

//Inital
float start_fan_speed = 35;
float start_fuel = 1;

//Full Power 
float final_fan_speed = 100.00;
float final_fuel = 3.0;
int full_power_increment_time = 30; //seconds

//Throttling_Power
float throttling_high_fuel = 3.5;
float throttling_low_fuel = 2.2;

//Fuel Mixture 

//Serial Settings
String message = "Off";
bool pushed;
bool long_press;
bool push;
bool debug_glow_plug_on = 2;
int debug_water_percent_map = 999;

//Varaiables
float fan_speed; // percent
float water_pump_speed; // percent
float fuel_need; // percent
int glow_time; // seconds
float water_temp; // degres C
float exhaust_temp; // degres C
float exhaust_temp_sec[10]; // array of last 10 sec water temp, degres C
int shower_timeout; 
int glow_left = 0;
int last_glow_value = 0;
bool shower;
bool burn;
bool webasto_fail;
bool cold_shower;
bool lean_burn;
int delayed_period = 0;
unsigned long water_pump_started_on;
int water_pump_started = 0;
long glowing_on = 0;
int burn_mode = 0;
//Varaiables

//Shower Config 
float shower_target = 39; // degrees C
float shower_min = 37; // minimal temperature for shower, degres C
int water_overheat = 50;// degrees C
int water_warning = 44;// degrees C
//Shower Config 


void setup() {

  TCCR1B = TCCR1B & 0b11111000 | 0x01;  // magic Fast PWM parameter

  pinMode(glow_plug_pin, OUTPUT);
  pinMode(fuel_pump_pin, OUTPUT);
  pinMode(burn_fan_pin, OUTPUT);
  pinMode(water_pump_pin, OUTPUT);
  pinMode(water_temp_pin, INPUT);
  pinMode(exhaust_temp_pin, INPUT);
  pinMode(push_pin, INPUT); // important, this pulls de button pin to high, so when pressed it goes to low // debug changed this to input from pullup

  Serial.begin(115200);
}

void loop() { // runs over and over again, calling the functions one by one
  temp_data();
  control();
  shower_void();
  webasto();
}
