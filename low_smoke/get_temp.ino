float get_temp(int temp_pin) { // read a sensor value, smoothen it a bit and convert it to C degrees

  
   if(temp_pin==14)
    return get_wabasto_temp(temp_pin,0);

  if(high_temp_exhaust_sensor)
  {
    float temp = 0;
    for (int i = 0; i < 10; i++) {    
      temp += get_wabasto_temp(exhaust_temp_pin,1);

    }
    temp /= 10;
    if(temp>300)
      return 300;
    return temp;
  }
   

  float temp = 0;
  for (int i = 0; i < 20; i++) {    
    temp += analogRead(temp_pin);
  }
  temp /= 20;

  if (temp < 25 || temp > 1000) { // check if sensor is good
    return 300;
  } else {
    temp = log(((10240000 / temp) - 10000));
    temp = 1 / (0.001129148 + (0.000234125 + (0.0000000876741 * temp * temp )) * temp );
    temp = temp - 273.15;
    return temp;
  }
}
