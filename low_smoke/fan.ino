void burn_fan() {
  // the webasto fan runs on 10v so we need to that into account : pwm average of 14v for 12v = 0.85, 255 * 0.85 = 216
  int percent_map = mapf(fan_speed, 0, 100, 0, 235); // pwm average of 14v for 12v = 0.85, 255 * 0.85 = 216

  analogWrite(burn_fan_pin, percent_map);
}
