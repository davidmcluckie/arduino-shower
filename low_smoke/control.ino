void control() {
  static unsigned long timer;
  if (millis() < timer) {
    timer = millis();
  }
  push = 0;

  if (!digitalRead(push_pin)) {
    push = 0;
  } else {
    push = 1;
  }

  if(millis() < 5000)
    push = 0;

  if (!push && !pushed) {
    timer = millis();
  }

  if (push && !pushed) {
    timer = millis();
    pushed = 1;
  }
  

  if (push && pushed && millis() > timer + 2000) { // debug increased this long delay to 2000 from 500
    if (!long_press) { // when long press
      long_press = 1;

      if (webasto_fail) { // reset webasto fail if there has been a failure
        webasto_fail = 0;

      } else {
        if (shower || cold_shower) { // during a shower, increase de timeout by 40 seconds
          shower_timeout += 40;

        } else { // if shower is off, start a cold shower with a 30 seconds timeout
          cold_shower = 1;
          shower_timeout = 30;
        }
      }

    }
  }

  if (push && pushed && millis() > timer + 5000) {
    if(burn_mode>0 && burn_mode<3)
    {
      burn = 0;
      shower = 0;
    }
  }

  //Remove later / update
  if (push && pushed && millis() > timer + 10000 && burn_mode == 0) {
    int count = 0;
    while(true)
    {
      digitalWrite(fuel_pump_pin, HIGH);
      delay(75);
      digitalWrite(fuel_pump_pin, LOW);
      delay(75);
      if(count>300)
        break;
      count++;
    } 
  }


  if (pushed && !push) {
    if (millis() > timer + 50 && !long_press) { // when short press

      if (cold_shower) { // if shower if on, turn it off, with a delay of 15 seconds if it was hot
        shower_timeout = 0;
      } else if (shower) {
        shower_timeout = 15; // this delay helps to burn all the remaining fuel, as it will switch on lean_burn

      } else { // if shower is off, turn it on with a 60 seconds timeout
        shower = 1;
        shower_timeout = 60;
      }
    }
    pushed = 0;
    long_press = 0;
  }
}
