//Regulates pump flow 
void shower_void() {
  static unsigned long secs_timer;
  if (millis() < secs_timer) {
    secs_timer = millis();
  }
  static int seconds;

  if (shower) {
    burn = 1;
    if (burn_mode != 1 && (exhaust_temp > exhaust_temp_sec[9] + 1 || exhaust_temp > 30)) { // if getting hot

      if (water_temp < shower_min) {
        water_pump_speed = 91; // TODO i think it's 90-100% scaled due to the small voltage range of control
        // starts circulating water slowly, otherwise it could boil inside the webasto

      } else if (water_temp - shower_target > -2) { // if water temp gets really close to the target
        water_pump_speed = mapf(water_temp, shower_target - 1, shower_target + 5, 91, 100);
        // the mapf fonction will regulate the flow betwen 10% and 60% depending on how close water temp is to the targe

      } else if (water_temp - shower_target > 0.5) { // if water getting to hot
        water_pump_speed = mapf(water_temp, shower_target + 1 , shower_target + 5, 91, 100);
        // this it to prevent water boiling inside the webasto
      }

    } else {
      water_pump_speed = 0;
    }

    if(burn_mode == 1 && water_temp > water_warning)
      water_pump_speed = 100; 
  }
  else { // reinitialize variables
    burn = 0;
    water_pump_speed = 0;
    seconds = 0;
    shower_timeout = 0;
    lean_burn = 0;
  }

  water_pump(); // calls the water_pump function
}
