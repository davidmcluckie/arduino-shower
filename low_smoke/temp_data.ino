void temp_data() { // keeps the temp variables updated
  static unsigned long timer;
  if (millis() < timer) {
    timer = millis();
  }
  // call the get_temp function and smoothen the result
  water_temp = (9 * water_temp + get_temp(water_temp_pin)) / 10; 
  exhaust_temp = (9 * exhaust_temp + get_temp(exhaust_temp_pin)) / 10;

  if (millis() > timer + 1000) { // every sec
    timer = millis();
      
    for ( int i = 9; i >= 1 ; i-- ) { // updating the exhaust temperature history
      exhaust_temp_sec[i] = exhaust_temp_sec[i - 1]; // shift array values, deleting the older one
    }
    exhaust_temp_sec[0] = exhaust_temp; // add new temp value
  }
}
