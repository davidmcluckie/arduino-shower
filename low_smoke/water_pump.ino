void water_pump() {
  //Maps speed
  int percent_map = mapf(water_pump_speed, 0, 100, 0, 255);
  static bool overheat;

  // if serious overheat
  if (exhaust_temp > 400 || water_temp > 60 || (overheat && water_temp > 40)) {
    overheat = 1;
    percent_map = 255;
    webasto_fail = 1;
  } else if (overheat && water_temp <= water_warning) {
    overheat = 0;
    percent_map = 0;
  }

  if(burn_mode >= 1 && burn_mode <= 3 && water_temp > water_warning)
    percent_map = 255; 
  else if(burn_mode == 1 && water_temp > 15)
    percent_map = 232; 

  if(water_pump_started == 0 && water_pump_speed > 0)
  {
    water_pump_started_on = millis();
    water_pump_started = 1;
  }
  else if(water_pump_speed == 0)
    water_pump_started = 0;

  if(water_pump_started == 1 && (millis()-water_pump_started_on) <= 2000)
    percent_map = 255; 
  else
    water_pump_started_on = 0;

  debug_water_percent_map = percent_map;

  analogWrite(water_pump_pin, percent_map);
}
